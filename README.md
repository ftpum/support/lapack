# Quadruple Precision LAPACK

The `qp` branch builds a quadruple precision of LAPACK. Use the `./build.sh` to
build it.
