#!/bin/bash

set -e
set -x

mkdir build_quad
cd build_quad
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_Fortran_FLAGS="-fdefault-real-8 -march=native -Ofast -funroll-loops" -DBUILD_SINGLE=OFF -DBUILD_COMPLEX=OFF -DBUILD_COMPLEX16=OFF  ..
make

cd lib
nm -g liblapack.a  | grep -o " T .*" | cut -f2- "-dT" | cut -f2- "-d " > symbols.txt
nm -g libblas.a  | grep -o " T .*" | cut -f2- "-dT" | cut -f2- "-d " >> symbols.txt

sed 's|\(.*$\)|\1 quad_\1|g' symbols.txt > symbols.def
sort -u symbols.def -o symbols2.def

objcopy --redefine-syms symbols2.def libblas.a
objcopy --redefine-syms symbols2.def liblapack.a

mv libblas.a libblas_quad.a
mv liblapack.a liblapack_quad.a
